# First step: Build with Node.js
FROM node:alpine AS Builder
WORKDIR /app
COPY package.json  package-lock.json /app/
RUN npm install
COPY . /app
RUN npm run build

# Deliver the dist folder with Nginx
FROM nginx:alpine
COPY --from=Builder /app/dist /usr/share/nginx/html/

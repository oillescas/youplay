import Vue from 'vue';
import Vuetify from 'vuetify';
import VueResource from 'vue-resource';
import VueYouTubeEmbed from 'vue-youtube-embed';

import router from './router';
import App from './App';
import store from './store';

import 'vuetify/dist/vuetify.min.css';
import './css/app.css';

// Setup dependencies
Vue.use(VueResource);
Vue.use(Vuetify);
Vue.use(VueYouTubeEmbed);


new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
});
import Vue from 'vue';
import Vuex from 'vuex';
import youtube from './modules/youtube';
import player from './modules/player';
import ux from './modules/ux';


Vue.use(Vuex);


const modules = {
  youtube,
  player,
  ux
};

export default new Vuex.Store({
  modules
});

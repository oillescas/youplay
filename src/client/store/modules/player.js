
// initial state
const state = {
  video:  {id:'GMFewiplIbw'},
  playlist:[]
};

// getters
const getters = {};

const actions = {
  changeVideo ({ commit }, video) {
    //dispatch('someOtherAction') // -> 'foo/someOtherAction'
    //dispatch('someOtherAction', null, { root: true }) // -> 'someOtherAction'
    commit('setVideoId', video);
  },
  addVideo  ({ commit }, video) {
    commit('newVideo', video);
  },
  onEnded({ state, dispatch }){
    const indiceActual = state.playlist.indexOf(state.video) || 0;
    if(state.playlist.length > 0){
      dispatch('changeVideo',state.playlist[indiceActual+1]);
    }
  },
  removeVideo({ commit }, video){
    commit('delVideo', video);
  },
  clearList({commit}){
    commit('clean');
  }
};

const mutations = {
  setVideoId(state, status){
    state.video = status;
  },
  newVideo(state,status){
    state.playlist.push(status);
  },
  delVideo(state,status){
    state.playlist.splice(state.playlist.indexOf(status), 1);
  },
  clean(state){
    state.playlist = [];
  }
};



export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

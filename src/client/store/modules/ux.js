// initial state
const state = {
  drawer:false
};


// getters
const getters = {};


const actions = {
  toggleDrawer: function({ commit, state }){
    commit('setDrawer',!state.drawer);
  }
};


const mutations = {
  setDrawer(state, newDrawer){
    console.log("aqui llega", newDrawer);
    state.drawer = newDrawer;
  }
};



export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

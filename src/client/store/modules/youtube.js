import YouTube from 'simple-youtube-api';
const youtube = new YouTube('AIzaSyAI_n_kLD2WTT-i_pN11enq8JAYB68jBB4');


// initial state
const state = {
  q: 'Becky G',
  searchResults: [],
  relatedVideos: []
};

// getters
const getters = {};

// actions
const actions = {
  getAllVideos({ commit , state}) {
    youtube.searchVideos(state.q, 20)
      .then(results => {
        commit('setSearchResults', results);
      })
      .catch(console.log);
  },
  getRelatedVideos({ commit }, videoId) {
    youtube.searchVideos('',10,{relatedToVideoId:videoId}).then(results => {
      commit('setRelatedResults', results);
    })
      .catch(console.log);

  }
};


// mutations
const mutations = {
  setSearchResults(state, videos) {
    state.searchResults = videos;
  },
  setRelatedResults(state, videos){
    state.relatedVideos = videos;
  },
  updateSearch (state, message) {
    state.q = message;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
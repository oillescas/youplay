const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

app.use(express.static('dist'));

app.get('/api/time', (req, res) => {
  res.send({ now:new Date() });
});

app.listen(8001, () => console.log('Listening on port 8001!'));